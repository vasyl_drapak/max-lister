#ifndef UTILS_H
#define UTILS_H
#include <string>

#if defined(_WIN32)

#   include <shutil.h>
#   include <shlobj.h>

#   if WINVER < 0x0600
#           define GetFolderPath SHGetFolderPath
#           define USER_HOME CSIDL_USER_PROFILE
#   else
#       define GetFolderPath SHGetKnownFolderPath
#       define USER_HOME
#   endif

#   define DIR_SEP '\\'
#else

#   include <unistd.h>
#   include <sys/types.h>
#   include <stdlib.h>
#   include <pwd.h>

#   define DIR_SEP '/'

#endif



std::string getHomedir();
bool isPlatformRoot(const std::string &_path);
void platformUp(std::string& path);
void platformDown(std::string &path, const std::string &child);

#endif // UTILS_H

