#include "lister.h"
#include <dirent.h>
#include <string.h>

namespace max {
using namespace v8;

Persistent<Function> Lister::constructor;

Lister::Lister(const std::string& path) : _path(path) {
}

Lister::~Lister() {
}

void Lister::Init(v8::Local<v8::Object> exports) {
    Isolate* isolate = exports->GetIsolate();
    // Prepare constructor template
    Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
    tpl->SetClassName(String::NewFromUtf8(isolate, "Lister"));
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    // Prototype
    NODE_SET_PROTOTYPE_METHOD(tpl, "isRoot",     isRoot);
    NODE_SET_PROTOTYPE_METHOD(tpl, "currPath",   currPath);
    NODE_SET_PROTOTYPE_METHOD(tpl, "up",         up);
    NODE_SET_PROTOTYPE_METHOD(tpl, "chDir",      chDir);
    NODE_SET_PROTOTYPE_METHOD(tpl, "changePath", changePath);
    NODE_SET_PROTOTYPE_METHOD(tpl, "list",       list);

    constructor.Reset(isolate, tpl->GetFunction());
    exports->Set(String::NewFromUtf8(isolate, "Lister"),
                 tpl->GetFunction());
}

void Lister::New(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    if (args.IsConstructCall()) {
        // Invoked as constructor: `new Lister(...)`
        Lister* obj = new Lister(getHomedir());
        obj->Wrap(args.This());
        args.GetReturnValue().Set(args.This());
    } else {
        // Invoked as plain function `Lister(...)`, turn into construct call.
        const int argc = 1;
        Local<Value> argv[argc] = { args[0] };
        Local<Function> cons = Local<Function>::New(isolate, constructor);
        args.GetReturnValue().Set(cons->NewInstance(argc, argv));
    }
}

void Lister::NewInstance(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    const unsigned argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> instance = cons->NewInstance(argc, argv);

    args.GetReturnValue().Set(instance);
}

void Lister::isRoot(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());
    args.GetReturnValue().Set(Boolean::New(isolate, isPlatformRoot(obj->_path)));
}

void Lister::currPath(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());

    args.GetReturnValue().Set(String::NewFromUtf8(isolate, obj->_path.c_str()));
}

void Lister::up(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());
    if(isPlatformRoot(obj->_path))
        return;
    platformUp(obj->_path);
    args.GetReturnValue().Set(Undefined(isolate));
}

void Lister::chDir(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());
    /*if (args.Length() != 1 || !args[1]->IsString()) {
        ThrowException(Exception::TypeError(String::New("Arguments 1 expect a string!")));
        args.GetReturnValue().Set(Undefined(isolate));
    }*/
    auto param = args[0]->ToString();
    uint8_t* buffer = new uint8_t[param->Length() + 1];
    param->WriteOneByte(buffer);
    platformDown(obj->_path, std::string((char*) buffer));

    delete [] buffer;
    args.GetReturnValue().Set(Undefined(isolate));
}

void Lister::changePath(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());
    /*if (args.Length() != 1 || !args[1]->IsString()) {
        ThrowException(Exception::TypeError(String::New("Arguments 1 expect a string!")));
        args.GetReturnValue().Set(Undefined(isolate));
    }*/
    auto param = args[0]->ToString();
    uint8_t* buffer = new uint8_t[param->Length() + 1];
    param->WriteOneByte(buffer);

    obj->_path = std::string((char*) buffer);
    delete [] buffer;
    args.GetReturnValue().Set(Undefined(isolate));
}

void Lister::list(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    auto retVal = Object::New(isolate);
    auto files = Array::New(isolate), dirs = Array::New(isolate);
    Lister* obj = node::ObjectWrap::Unwrap<Lister>(args.Holder());

    DIR* dir = opendir(obj->_path.c_str());
    dirent* entry;
    while((entry = readdir(dir))) {
        if(!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        if(entry->d_type&DT_DIR)
            dirs->Set(dirs->Length(), String::NewFromUtf8(isolate, entry->d_name));
        else
            files->Set(files->Length(), String::NewFromUtf8(isolate, entry->d_name));
    }

    retVal->Set(String::NewFromUtf8(isolate, "files"), files);
    retVal->Set(String::NewFromUtf8(isolate, "dirs"), dirs);

    args.GetReturnValue().Set(retVal);
}


} // namespace max
