#ifndef LISTER_H
#define LISTER_H
#include <node/node.h>
#include <node/node_object_wrap.h>
#include <v8.h>
#include "utils.h"

namespace max {


using namespace v8;


class Lister : public node::ObjectWrap {
    std::string _path;

public:
    static void Init(v8::Local<v8::Object> exports);
    static void NewInstance(const FunctionCallbackInfo<v8::Value>& args);

    static void isRoot(const FunctionCallbackInfo<v8::Value>& args);
    static void currPath(const FunctionCallbackInfo<v8::Value>& args);
    static void up(const FunctionCallbackInfo<v8::Value>& args);
    static void chDir(const FunctionCallbackInfo<v8::Value>& args);
    static void changePath(const FunctionCallbackInfo<v8::Value>& args);
    static void list(const FunctionCallbackInfo<v8::Value>& args);



private:
    explicit Lister(const std::string& path);
    ~Lister();

    static void New(const FunctionCallbackInfo<v8::Value>& args);
    static Persistent<Function> constructor;
    friend class v8::Local<v8::Value>;
};


} // namespace max
#endif // LISTER_H
