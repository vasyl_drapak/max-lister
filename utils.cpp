#include "utils.h"

#ifdef _WIN32
std::string getHomedir() {
    return "";
}
bool isPlatformRoot(const std::string& _path) {
    return ( iswalpha(_path[0])) && _path[1] == ':' && _path[2] == '\\' );
}
#else
std::string getHomedir() {
   return std::string((getenv("HOME") == NULL) ?
                       getenv("HOME") :
                       getpwuid(getuid())->pw_dir);
}

bool isPlatformRoot(const std::string& _path) {
    return _path == "/";
}
#endif

void platformUp(std::string& path) {
    auto sepPos = path.rfind(DIR_SEP);
    //if(sepPos < 0)
      //  return; // if path is bad it`s not my problem

    if(sepPos == path.length() - 1) // separator is last symbol
        sepPos = path.rfind(DIR_SEP, sepPos - 1);

    path.resize(sepPos + 1);
}

void platformDown(std::string& path, const std::string& child) {
    if (path[path.length() - 1] != DIR_SEP)
        path += DIR_SEP;
    path += child;
    path += DIR_SEP;
}
