#include <v8.h>
#include <node/node.h>
using namespace v8;

/*
Handle<Value> Hello(const Arguments& args) {
    HandleScope scope;
    return scope.Close(String::New("Hello Bitch"));
}


void init(Handle<Object> target) {
    target->Set(String::NewSymbol("hello"),
                FunctionTemplate::New(Hello)->GetFunction());
    target->Set(String::NewSymbol("isMaxPidor"),
                Boolean::New(true));
}
*/
//NODE_MODULE(hello, init)
#include "lister.h"

namespace max {

using v8::Local;
using v8::Object;

void InitAll(Local<Object> exports) {
    Lister::Init(exports);
}

NODE_MODULE(lister, InitAll)

}  // namespace demo
